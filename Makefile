dc=docker-compose
certPath=$(PWD)/volumes/nginx/etc/nginx/certs

include .env
include .env.local

init: copy-env certs build up

build: build-node

copy-env:
	cp .env .env.local

run-sh:
	$(dc) -p 8081:8080 run node sh

build-node:
	docker build ./docker/node/ -t streaming-ui/node:latest --no-cache

sh:
	docker-compose exec node sh

nginx-sh:
	docker-compose exec nginx sh

lint:
	docker-compose exec node yarn vue-cli-service lint

up:
	$(dc) up -d

ups:
	$(dc) up

down:
	$(dc) down

certs:
	docker run --rm -d -e domain="$(SERVER_NAME)" \
        -v $(certPath):/root/.local/share/mkcert \
		vishnunair/docker-mkcert
	sleep 2
	mv $(certPath)/$(SERVER_NAME).pem $(certPath)/cert.pem
	mv $(certPath)/$(SERVER_NAME)-key.pem $(certPath)/key.pem
