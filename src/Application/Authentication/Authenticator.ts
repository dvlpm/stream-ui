import {Auth0Client, RedirectLoginResult} from "@auth0/auth0-spa-js";
import {injectable} from "tsyringe";

@injectable()
export default class Authenticator {
    private auth0Client!: Auth0Client;

    constructor() {
        this.auth0Client = new Auth0Client({
            domain: "dvlpm.auth0.com",
            client_id: "jRaKy7JlugUBCSxyn5si5MiMt0lZV9Fg",
            redirect_uri: "https://" + location.host + "/auth-callback",
            postLogoutRedirectUri: "https://" + location.host + "/",
        });
    }

    getAccessToken(): Promise<string> {
        return this.auth0Client.getTokenSilently();
    }

    loginWithRedirect(): Promise<void> {
        return this.auth0Client.loginWithRedirect();
    }

    handleCallback(): Promise<RedirectLoginResult> {
        return this.auth0Client.handleRedirectCallback();
    }
}
