import axios, {AxiosError, AxiosResponse} from "axios";

export default class ApiClient {
    get<S>(url: string, config?: any): Promise<S> {
        return axios.get(url, config)
            .then((response: AxiosResponse) => {
                let responseData = response.data as S;

                return Promise.resolve(responseData);
            })
            .catch((error: AxiosError) => {
                return Promise.reject(error)
            });
    }

    post<S>(url: string, data?: any, config?: any): Promise<S> {
        return axios.post(url, data, config).then((response: AxiosResponse) => {
            let responseData = response.data as S;

            return Promise.resolve(responseData);
        })
    }

    put<S>(url: string, data?: any, config?: any): Promise<S> {
        return axios.put(url, data, config).then((response: AxiosResponse) => {
            let responseData = response.data as S;

            return Promise.resolve(responseData);
        })
    }
}