export default class RequestConfigFactory {
    public create(id: string, token: string, params?: object): RequestConfigInterface {
        return {
            headers: {
                "Authorization": "Bearer " + token,
                "User-Id": id,
            },
            params
        } as RequestConfigInterface;
    }
}

export interface RequestConfigInterface {
    headers: object,
    params: object | null,
}
