import {inject, injectable} from "tsyringe";
import RequestConfigFactory from "@/Application/Factory/RequestConfigFactory";
import ApiClient from "@/Application/Api/ApiClient";
import HallState from "@/Domain/Space/State/HallState";
import Hall from "@/Domain/Space/Model/Hall";
import HallFactory from "@/Domain/Space/Factory/HallFactory";
import CreateHallPayload from "@/Domain/Space/Payload/CreateHallPayload";
import {HostRole} from "@/Domain/Space/Model/HostRole";

@injectable()
export default class HallService {
    constructor(
        @inject(ApiClient) private apiClient: ApiClient,
        @inject(HallFactory) private hallFactory: HallFactory,
        @inject(RequestConfigFactory) private requestConfigFactory: RequestConfigFactory
    ) {
    }

    async show(userId: string, accessToken: string, hallIdentifier: string): Promise<Hall> {
        const state = await this.apiClient.get<HallState>(
            '/api/hall/' + hallIdentifier,
            this.requestConfigFactory.create(userId, accessToken)
        );

        return this.hallFactory.createFromState(state);
    }

    async list(
        userId: string,
        accessToken: string,
        hostUserId: string | null = null,
        hostUserRoles: HostRole[] = [],
    ): Promise<Hall[]> {
        const states = await this.apiClient.get<HallState[]>(
            '/api/halls',
            this.requestConfigFactory.create(
                userId,
                accessToken,
                {
                    "host.user.id": hostUserId,
                    "host.user.role": hostUserRoles
                }
            )
        );

        return states.map((state: HallState) => {
            return this.hallFactory.createFromState(state);
        });
    }

    async create(userId: string, accessToken: string, payload: CreateHallPayload): Promise<Hall> {
        const state = await this.apiClient.post<HallState>(
            '/api/hall',
            payload,
            this.requestConfigFactory.create(userId, accessToken)
        );

        return this.hallFactory.createFromState(state);
    }
}
