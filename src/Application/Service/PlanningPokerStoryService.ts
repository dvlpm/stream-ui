import {inject, injectable} from "tsyringe";
import RequestConfigFactory from "@/Application/Factory/RequestConfigFactory";
import ApiClient from "@/Application/Api/ApiClient";
import PlanningPokerStory from "@/Domain/Content/Model/PlanningPokerStory";
import PlanningPokerStoryState from "@/Domain/Content/State/PlanningPokerStoryState";
import PlanningPokerStoryFactory from "@/Domain/Content/Factory/PlanningPokerStoryFactory";
import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";

@injectable()
export default class PlanningPokerStoryService {
    constructor(
        @inject(ApiClient) private apiClient: ApiClient,
        @inject(PlanningPokerStoryFactory) private planningPokerStoryFactory: PlanningPokerStoryFactory,
        @inject(RequestConfigFactory) private requestConfigFactory: RequestConfigFactory
    ) {
    }

    async addVote(
        userId: string,
        accessToken: string,
        planningPokerStoryId: string,
        card: PlanningPokerCard
    ): Promise<PlanningPokerStory> {
        const state = await this.apiClient.post<PlanningPokerStoryState>(
            '/api/planning-poker-story/' + planningPokerStoryId + '/vote',
            {
                card
            },
            this.requestConfigFactory.create(
                userId,
                accessToken
            )
        )

        return this.planningPokerStoryFactory.createFromState(state);
    }
}
