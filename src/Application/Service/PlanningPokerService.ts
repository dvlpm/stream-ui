import {inject, injectable} from "tsyringe";
import RequestConfigFactory from "@/Application/Factory/RequestConfigFactory";
import ApiClient from "@/Application/Api/ApiClient";
import PlanningPokerStory from "@/Domain/Content/Model/PlanningPokerStory";
import PlanningPokerStoryState from "@/Domain/Content/State/PlanningPokerStoryState";
import PlanningPokerStoryFactory from "@/Domain/Content/Factory/PlanningPokerStoryFactory";

@injectable()
export default class PlanningPokerService {
    constructor(
        @inject(ApiClient) private apiClient: ApiClient,
        @inject(PlanningPokerStoryFactory) private planningPokerStoryFactory: PlanningPokerStoryFactory,
        @inject(RequestConfigFactory) private requestConfigFactory: RequestConfigFactory
    ) {
    }

    async listStories(
        userId: string,
        accessToken: string,
        planningPokerId: string,
        limit: number = 5,
        offset: number = 0,
        status?: string,
    ): Promise<PlanningPokerStory[]> {
        const states = await this.apiClient.get<PlanningPokerStoryState[]>(
            '/api/planning-poker/' + planningPokerId + '/stories',
            this.requestConfigFactory.create(
                userId,
                accessToken,
                {
                    limit,
                    offset,
                    status
                }
            )
        )

        return states.map((state: PlanningPokerStoryState) => {
            return this.planningPokerStoryFactory.createFromState(state);
        });
    }
}
