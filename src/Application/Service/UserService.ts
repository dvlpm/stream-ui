import {inject, injectable} from "tsyringe";
import RequestConfigFactory from "@/Application/Factory/RequestConfigFactory";
import User from "@/Domain/User/Model/User";
import UserState from "@/Domain/User/State/UserState";
import ApiClient from "@/Application/Api/ApiClient";
import UserFactory from "@/Domain/User/Factory/UserFactory";

@injectable()
export default class UserService {
    constructor(
        @inject(ApiClient) private apiClient: ApiClient,
        @inject(UserFactory) private userFactory: UserFactory,
        @inject(RequestConfigFactory) private requestConfigFactory: RequestConfigFactory
    ) {
    }

    async me(userId: string, accessToken: string): Promise<User> {
        const state = await this.apiClient.get<UserState>(
            '/api/user/me',
            this.requestConfigFactory.create(userId, accessToken)
        )

        return this.userFactory.createFromState(state);
    }
}
