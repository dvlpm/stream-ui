import {inject, injectable} from "tsyringe";
import RequestConfigFactory from "@/Application/Factory/RequestConfigFactory";
import ApiClient from "@/Application/Api/ApiClient";
import RoomState from "@/Domain/Space/State/RoomState";
import Room from "@/Domain/Space/Model/Room";
import RoomFactory from "@/Domain/Space/Factory/RoomFactory";

@injectable()
export default class RoomService {
    constructor(
        @inject(ApiClient) private apiClient: ApiClient,
        @inject(RoomFactory) private roomFactory: RoomFactory,
        @inject(RequestConfigFactory) private requestConfigFactory: RequestConfigFactory
    ) {
    }

    async show(userId: string, accessToken: string, roomId: string): Promise<Room> {
        const state = await this.apiClient.get<RoomState>(
            '/api/room/' + roomId,
            this.requestConfigFactory.create(userId, accessToken)
        )

        return this.roomFactory.createFromState(state);
    }
}
