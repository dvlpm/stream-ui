import {PlanningPokerType} from "@/Domain/Content/Model/PlanningPokerType";
import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";

export default class PlanningPokerDataState {
    constructor(
        public type: PlanningPokerType,
        public cards: PlanningPokerCard[],
    ) {
    }
}
