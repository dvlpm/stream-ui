import AbstractContentState from "@/Domain/Content/State/AbstractContentState";
import {ContentType} from "@/Domain/Content/Model/ContentType";

export default class YouTubeStreamState extends AbstractContentState{
    constructor(
        id: string,
        public link: string
    ) {
        super(id, ContentType.YOUTUBE_STREAM);
    }
}
