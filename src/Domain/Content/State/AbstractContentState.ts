import {ContentType} from "@/Domain/Content/Model/ContentType";

export default class AbstractContentState {
    constructor(
        public id: string,
        public type: ContentType,
    ) {
    }
}