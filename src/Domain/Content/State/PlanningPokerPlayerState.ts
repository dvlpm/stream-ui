import UserState from "@/Domain/User/State/UserState";
import {PlanningPokerPlayerStatus} from "@/Domain/Content/Model/PlanningPokerPlayerStatus";

export default class PlanningPokerPlayerState {
    constructor(
        public id: string,
        public user: UserState,
        public status: PlanningPokerPlayerStatus
    ) {
    }
}
