import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";
import UserState from "@/Domain/User/State/UserState";

export default class PlanningPokerStoryVoteState {
    constructor(
        public card: PlanningPokerCard,
        public user: UserState,
    ) {
    }
}
