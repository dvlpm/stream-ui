import AbstractContentState from "@/Domain/Content/State/AbstractContentState";
import {ContentType} from "@/Domain/Content/Model/ContentType";
import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";
import PlanningPokerPlayerState from "@/Domain/Content/State/PlanningPokerPlayerState";

export default class PlanningPokerState extends AbstractContentState {
    constructor(
        id: string,
        public cards: PlanningPokerCard[],
        public players: PlanningPokerPlayerState[],
    ) {
        super(id, ContentType.PLANNING_POKER);
    }
}