import {PlanningPokerStoryStatus} from "@/Domain/Content/Model/PlanningPokerStoryStatus";
import PlanningPokerStoryVoteState from "@/Domain/Content/State/PlanningPokerStoryVoteState";

export default class PlanningPokerStoryState {
    constructor(
        public id: string,
        public name: string,
        public order: number,
        public score: string,
        public status: PlanningPokerStoryStatus,
        public description: string,
        public link: string,
        public votes: PlanningPokerStoryVoteState[]
    ) {
    }
}
