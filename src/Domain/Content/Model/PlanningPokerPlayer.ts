import User from "@/Domain/User/Model/User";
import {PlanningPokerPlayerStatus} from "@/Domain/Content/Model/PlanningPokerPlayerStatus";

export default class PlanningPokerPlayer {
    constructor(
        private _id: string,
        private _user: User,
        private _state: PlanningPokerPlayerStatus
    ) {
    }

    get user(): User {
        return this._user;
    }
}
