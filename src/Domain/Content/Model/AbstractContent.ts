export default abstract class AbstractContent {
    protected constructor(
        protected _id: string
    ) {
    }

    get id(): string {
        return this._id;
    }
}
