import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";
import User from "@/Domain/User/Model/User";

export default class PlanningPokerStoryVote {
    constructor(
        private _card: PlanningPokerCard,
        private _user: User
    ) {
    }

    get card(): PlanningPokerCard {
        return this._card;
    }

    get user(): User {
        return this._user;
    }
}
