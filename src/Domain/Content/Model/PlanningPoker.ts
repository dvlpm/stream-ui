import AbstractContent from "@/Domain/Content/Model/AbstractContent";
import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";
import PlanningPokerStory from "@/Domain/Content/Model/PlanningPokerStory";
import PlanningPokerPlayer from "@/Domain/Content/Model/PlanningPokerPlayer";

export default class PlanningPoker extends AbstractContent {
    public constructor(
        id: string,
        private _cards: PlanningPokerCard[],
        private _players: PlanningPokerPlayer[],
        private _stories: PlanningPokerStory[] = [],
    ) {
        super(id);
    }

    addStories(...stories: PlanningPokerStory[]): PlanningPoker
    {
        const storiesMap = new Map<string, PlanningPokerStory>();

        this._stories.map((story: PlanningPokerStory) => {
            storiesMap.set(story.id, story);
        });

        stories.map((story: PlanningPokerStory) => {
            storiesMap.set(story.id, story);
        });

        this._stories = Array.from(storiesMap.values());

        return this;
    }

    get cards(): PlanningPokerCard[] {
        return this._cards;
    }

    get players(): PlanningPokerPlayer[] {
        return this._players;
    }

    get backlogStories(): PlanningPokerStory[] {
        return this._stories.filter((story: PlanningPokerStory) => {
            return story.isBacklog;
        });
    }

    get completedStories(): PlanningPokerStory[] {
        return this._stories.filter((story: PlanningPokerStory) => {
            return story.isCompleted;
        });
    }

    get activeStory(): PlanningPokerStory | null {
        const activeStories = this._stories.filter((story: PlanningPokerStory) => {
            return story.isActive;
        });

        return activeStories.shift() ?? null;
    }
}
