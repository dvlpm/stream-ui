import AbstractContent from "@/Domain/Content/Model/AbstractContent";
import YouTubeStreamState from "@/Domain/Content/State/YouTubeStreamState";

export default class YouTubeStream extends AbstractContent {
    public constructor(
        id: string,
        private _link: string
    ) {
        super(id);
    }

    get link(): string {
        return this._link;
    }
}
