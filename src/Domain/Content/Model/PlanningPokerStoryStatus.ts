export enum PlanningPokerStoryStatus {
    BACKLOG = 'BACKLOG',
    ACTIVE = 'ACTIVE',
    COMPLETED = 'COMPLETED',
}
