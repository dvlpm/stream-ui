export enum PlanningPokerPlayerStatus {
    OBSERVER = 'OBSERVER',
    ACTIVE = 'ACTIVE',
}
