import {PlanningPokerStoryStatus} from "@/Domain/Content/Model/PlanningPokerStoryStatus";
import PlanningPokerStoryVote from "@/Domain/Content/Model/PlanningPokerStoryVote";

export default class PlanningPokerStory {
    constructor(
        private _id: string,
        private _name: string,
        private _order: number,
        private _score: string,
        private _status: PlanningPokerStoryStatus,
        private _description: string,
        private _link: string,
        private _votes: PlanningPokerStoryVote[]
    ) {
    }

    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get status(): PlanningPokerStoryStatus {
        return this._status;
    }

    get votes(): PlanningPokerStoryVote[] {
        return this._votes;
    }

    get isCompleted(): boolean {
        return this._status === PlanningPokerStoryStatus.COMPLETED;
    }

    get isBacklog(): boolean {
        return this._status === PlanningPokerStoryStatus.BACKLOG;
    }

    get isActive(): boolean {
        return this._status === PlanningPokerStoryStatus.ACTIVE;
    }
}