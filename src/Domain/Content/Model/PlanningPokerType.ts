export enum PlanningPokerType {
    SCRUM = 'SCRUM',
    FIBONACCI = 'FIBONACCI',
    SEQUENTIAL = 'SEQUENTIAL',
    PLAYING_CARDS = 'PLAYING_CARDS',
    T_SHIRT = 'T_SHIRT',
}
