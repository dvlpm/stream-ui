import PlanningPokerState from "@/Domain/Content/State/PlanningPokerState";
import PlanningPoker from "@/Domain/Content/Model/PlanningPoker";
import {inject, injectable} from "tsyringe";
import PlanningPokerPlayerFactory from "@/Domain/Content/Factory/PlanningPokerPlayerFactory";
import PlanningPokerPlayerState from "@/Domain/Content/State/PlanningPokerPlayerState";

@injectable()
export default class PlanningPokerFactory {
    constructor(
        @inject(PlanningPokerPlayerFactory) private planningPokerPlayerFactory: PlanningPokerPlayerFactory
    ) {
    }

    createFromState(state: PlanningPokerState): PlanningPoker {
        return new PlanningPoker(
            state.id,
            state.cards,
            state.players.map((playerState: PlanningPokerPlayerState) => {
                return this.planningPokerPlayerFactory.createFromState(playerState);
            })
        )
    }
}
