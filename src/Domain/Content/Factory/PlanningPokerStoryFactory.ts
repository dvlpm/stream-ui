import PlanningPokerStoryState from "@/Domain/Content/State/PlanningPokerStoryState";
import PlanningPokerStoryVoteState from "@/Domain/Content/State/PlanningPokerStoryVoteState";
import PlanningPokerStory from "@/Domain/Content/Model/PlanningPokerStory";
import {inject, injectable} from "tsyringe";
import PlanningPokerStoryVoteFactory from "@/Domain/Content/Factory/PlanningPokerStoryVoteFactory";

@injectable()
export default class PlanningPokerStoryFactory {
    constructor(
        @inject(PlanningPokerStoryVoteFactory) private planningPokerStoryVoteFactory: PlanningPokerStoryVoteFactory,
    ) {
    }

    createFromState(state: PlanningPokerStoryState): PlanningPokerStory {
        return new PlanningPokerStory(
            state.id,
            state.name,
            state.order,
            state.score,
            state.status,
            state.description,
            state.link,
            state.votes.map((voteState: PlanningPokerStoryVoteState) => {
                return this.planningPokerStoryVoteFactory.createFromState(voteState);
            })
        );
    }
}
