import AbstractContentState from "@/Domain/Content/State/AbstractContentState";
import AbstractContent from "@/Domain/Content/Model/AbstractContent";
import {ContentType} from "@/Domain/Content/Model/ContentType";
import PlanningPokerState from "@/Domain/Content/State/PlanningPokerState";
import YouTubeStreamState from "@/Domain/Content/State/YouTubeStreamState";
import {inject, injectable} from "tsyringe";
import PlanningPokerFactory from "@/Domain/Content/Factory/PlanningPokerFactory";
import YouTubeStreamFactory from "@/Domain/Content/Factory/YouTubeStreamFactory";

@injectable()
export default class ContentFactory {
    constructor(
        @inject(PlanningPokerFactory) private planningPokerFactory: PlanningPokerFactory,
        @inject(YouTubeStreamFactory) private youTubeStreamFactory: YouTubeStreamFactory,
    ) {
    }

    createFromState(state: AbstractContentState): AbstractContent {
        if (state.type === ContentType.PLANNING_POKER) {
            return this.planningPokerFactory.createFromState(state as PlanningPokerState);
        }

        if (state.type === ContentType.YOUTUBE_STREAM) {
            return this.youTubeStreamFactory.createFromState(state as YouTubeStreamState);
        }

        throw new Error();
    }
}
