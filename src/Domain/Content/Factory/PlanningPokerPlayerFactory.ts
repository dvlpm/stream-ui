import {inject, injectable} from "tsyringe";
import UserFactory from "@/Domain/User/Factory/UserFactory";
import PlanningPokerPlayer from "@/Domain/Content/Model/PlanningPokerPlayer";
import PlanningPokerPlayerState from "@/Domain/Content/State/PlanningPokerPlayerState";

@injectable()
export default class PlanningPokerPlayerFactory {
    constructor(
        @inject(UserFactory) private userFactory: UserFactory
    ) {
    }

    createFromState(state: PlanningPokerPlayerState): PlanningPokerPlayer {
        return new PlanningPokerPlayer(
            state.id,
            this.userFactory.createFromState(state.user),
            state.status
        );
    }
}
