import YouTubeStreamState from "@/Domain/Content/State/YouTubeStreamState";
import YouTubeStream from "@/Domain/Content/Model/YouTubeStream";
import {injectable} from "tsyringe";

@injectable()
export default class YouTubeStreamFactory {
    createFromState(state: YouTubeStreamState): YouTubeStream {
        return new YouTubeStream(
            state.id,
            state.link
        );
    }
}
