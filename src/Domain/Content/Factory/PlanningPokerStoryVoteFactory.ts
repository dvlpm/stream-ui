import PlanningPokerStoryVoteState from "@/Domain/Content/State/PlanningPokerStoryVoteState";
import PlanningPokerStoryVote from "@/Domain/Content/Model/PlanningPokerStoryVote";
import {inject, injectable} from "tsyringe";
import UserFactory from "@/Domain/User/Factory/UserFactory";

@injectable()
export default class PlanningPokerStoryVoteFactory {
    constructor(
        @inject(UserFactory) private userFactory: UserFactory,
    ) {
    }

    createFromState(state: PlanningPokerStoryVoteState): PlanningPokerStoryVote {
        return new PlanningPokerStoryVote(
            state.card,
            this.userFactory.createFromState(state.user)
        );
    }
}
