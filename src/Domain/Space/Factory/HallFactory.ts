import HallState from "@/Domain/Space/State/HallState";
import RoomState from "@/Domain/Space/State/RoomState";
import Hall from "@/Domain/Space/Model/Hall";
import {inject, injectable} from "tsyringe";
import RoomFactory from "@/Domain/Space/Factory/RoomFactory";

@injectable()
export default class HallFactory {
    constructor(
        @inject(RoomFactory) private roomFactory: RoomFactory
    ) {
    }

    createFromState(state: HallState): Hall {
        return new Hall(
            state.identifier,
            state.name,
            state.description,
            state.rooms.map((roomState: RoomState) => {
                return this.roomFactory.createFromState(roomState);
            })
        );
    }
}
