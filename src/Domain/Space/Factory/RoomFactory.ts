import RoomState from "@/Domain/Space/State/RoomState";
import AbstractContentState from "@/Domain/Content/State/AbstractContentState";
import ContentFactory from "@/Domain/Content/Factory/ContentFactory";
import ParticipantState from "@/Domain/Space/State/ParticipantState";
import Room from "@/Domain/Space/Model/Room";
import {inject, injectable} from "tsyringe";
import ParticipantFactory from "@/Domain/Space/Factory/ParticipantFactory";

@injectable()
export default class RoomFactory {
    constructor(
        @inject(ParticipantFactory) private participantFactory: ParticipantFactory,
        @inject(ContentFactory) private contentFactory: ContentFactory,
    ) {
    }

    createFromState(state: RoomState): Room {
        return new Room(
            state.id,
            state.name,
            state.accessType,
            state.contents.map((contentState: AbstractContentState) => {
                return this.contentFactory.createFromState(contentState);
            }),
            state.description,
            state.participants.map((participantState: ParticipantState) => {
                return this.participantFactory.createFromState(participantState);
            })
        );
    }
}
