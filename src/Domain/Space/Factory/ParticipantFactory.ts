import ParticipantState from "@/Domain/Space/State/ParticipantState";
import Participant from "@/Domain/Space/Model/Participant";
import {inject, injectable} from "tsyringe";
import UserFactory from "@/Domain/User/Factory/UserFactory";

@injectable()
export default class ParticipantFactory {
    constructor(
        @inject(UserFactory) private userFactory: UserFactory
    ) {
    }

    createFromState(state: ParticipantState): Participant {
        return new Participant(
            state.id,
            state.user ? this.userFactory.createFromState(state.user) : null
        );
    }
}