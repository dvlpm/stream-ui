import UserState from "@/Domain/User/State/UserState";

export default class ParticipantState {
    constructor(
        public id: string,
        public user: UserState
    ) {
    }
}
