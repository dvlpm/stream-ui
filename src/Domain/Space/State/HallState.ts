import RoomState from "@/Domain/Space/State/RoomState";

export default class HallState {
    constructor(
        public identifier: string,
        public name: string,
        public description: string | null,
        public rooms: RoomState[],
    ) {
    }
}
