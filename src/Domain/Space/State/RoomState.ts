import {AccessType} from "@/Domain/Space/Model/AccessType";
import AbstractContentState from "@/Domain/Content/State/AbstractContentState";
import ParticipantState from "@/Domain/Space/State/ParticipantState";

export default class RoomState {
    constructor(
        public id: string,
        public name: string,
        public accessType: AccessType,
        public contents: AbstractContentState[] = [],
        public description: string | null,
        public participants: ParticipantState[] = []
    ) {
    }
}