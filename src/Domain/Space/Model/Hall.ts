import Room from "@/Domain/Space/Model/Room";

export default class Hall {
    public constructor(
        private _identifier: string,
        private _name: string,
        private _description: string | null,
        private _rooms: Room[],
    ) {
    }

    get identifier(): string {
        return this._identifier;
    }

    get name(): string {
        return this._name;
    }

    get rooms(): Room[] {
        return this._rooms;
    }

    addRooms(...rooms: Room[]): Hall {
        this._rooms.push(...rooms);

        return this;
    }
}
