export enum AccessType {
    ANONYMOUS = 'ANONYMOUS',
    AUTHORIZED = 'AUTHORIZED',
    LISTED = 'LISTED',
    PAID = 'PAID',
}
