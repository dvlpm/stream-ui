import User from "@/Domain/User/Model/User";

export default class Participant {
    constructor(
        private _id: string,
        private _user: User | null = null
    ) {
    }

    get id(): string {
        return this._id;
    }

    get user(): User | null {
        return this._user;
    }
}
