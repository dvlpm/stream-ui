import User from "@/Domain/User/Model/User";
import AbstractContent from "@/Domain/Content/Model/AbstractContent";
import {AccessType} from "@/Domain/Space/Model/AccessType";
import Participant from "@/Domain/Space/Model/Participant";
import PlanningPoker from "@/Domain/Content/Model/PlanningPoker";

export default class Room {
    public constructor(
        private _id: string,
        private _name: string,
        private _accessType: AccessType,
        private _contents: AbstractContent[],
        private _description: string | null,
        private _participants: Participant[]
    ) {
    }

    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get contents(): AbstractContent[] {
        return this._contents;
    }

    get participants(): Participant[] {
        return this._participants;
    }

    hasUser(user: User): boolean {
        return false;
    }

    addContents(...contents: AbstractContent[]) {
        this._contents.push(...contents);
    }

    findPlanningPoker(): PlanningPoker | null {
        return this.contents.find((content: AbstractContent) => {
            return content instanceof PlanningPoker;
        }) as PlanningPoker;
    }
}
