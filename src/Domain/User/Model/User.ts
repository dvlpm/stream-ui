import {v4 as uuidv4} from 'uuid';

export default class User {
    public constructor(
        private _id: string,
        private _role: string,
        private _name: string,
        private _username: string,
    ) {
    }

    static new(id?: string | null): User {
        return new User(
            !!id ? id : uuidv4(),
            'GUEST',
            '',
            ''
        );
    }

    get id(): string {
        return this._id
    }

    get isRegistered(): boolean {
        return this._role === 'USER'
    }

    equals(user: User): boolean {
        return this.id === user.id;
    }
}
