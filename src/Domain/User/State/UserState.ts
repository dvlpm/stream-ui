export default class UserState {
    constructor(
        public id: string,
        public role: string,
        public name: string,
        public username: string,
    ) {
    }
}
