import {injectable} from "tsyringe";
import UserState from "@/Domain/User/State/UserState";
import User from "@/Domain/User/Model/User";

@injectable()
export default class UserFactory {
    createFromState(state: UserState): User {
        return new User(
            state.id,
            state.role,
            state.name,
            state.username
        );
    }
}
