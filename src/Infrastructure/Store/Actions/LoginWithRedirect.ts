import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import Authenticator from "@/Application/Authentication/Authenticator";

@injectable()
export default class LoginWithRedirect implements ActionObject<State, State> {
    constructor(
        @inject(Authenticator) private authenticator: Authenticator
    ) {
    }

    handler: ActionHandler<State, State> = async () => {
        await this.authenticator.loginWithRedirect();
    }
}
