import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import HallService from "@/Application/Service/HallService";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import {HostRole} from "@/Domain/Space/Model/HostRole";

@injectable()
export default class LoadManagedHalls implements ActionObject<State, State> {

    constructor(
        @inject(HallService) private hallService: HallService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        const halls = await this.hallService.list(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            actionInjection.getters.userId,
            [
                HostRole.OWNER,
                HostRole.ADMINISTRATOR,
            ]
        );

        await actionInjection.commit(Mutations.managedHallsUpdated, halls);
    }
}
