import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import UserService from "@/Application/Service/UserService";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import {AxiosError} from "axios";
import ForgetMe from "@/Infrastructure/Store/Actions/User/ForgetMe";

@injectable()
export default class GetMe implements ActionObject<State, State> {

    constructor(
        @inject(UserService) private userService: UserService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        const currentUser = actionInjection.getters.user

        const user = await this.userService.me(
            currentUser.id,
            actionInjection.getters.accessToken
        ).catch((error: AxiosError) => {
            if (error.response?.status === 401) {
                actionInjection.dispatch(ForgetMe.name)
            }
        });

        if (!user) {
            return
        }

        if (user.id !== currentUser.id && !currentUser.isRegistered) {
            actionInjection.commit(Mutations.guestIdUpdated, actionInjection.getters.user.id)
        }

        actionInjection.commit(Mutations.userUpdated, user)
    }
}
