import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {injectable} from "tsyringe";
import router from "@/Infrastructure/Router/Router";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import User from "@/Domain/User/Model/User";

@injectable()
export default class ForgetMe implements ActionObject<State, State> {

    handler: ActionHandler<State, State>= async (actionInjection: ActionInjection) => {
        actionInjection.commit(Mutations.userUpdated, User.new(actionInjection.getters.guestId))
        actionInjection.commit(Mutations.accessTokenUpdated, '')

        router.push({name: 'auth-page'})
    }
}
