import {ActionHandler, ActionObject} from "vuex";
import {emptyState, State} from "@/Infrastructure/Store/State";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import {injectable} from "tsyringe";
import GetMe from "@/Infrastructure/Store/Actions/User/GetMe";

@injectable()
export default class InitializeState implements ActionObject<State, State> {

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        if (actionInjection.getters.isInitialized) {
            return;
        }

        let initialState = actionInjection.state;
        let state: State = initialState;

        if (localStorage.getItem('state') !== null) {
            // TODO https://jira.dvl.pm/browse/STREAM-54
            state = JSON.parse(<string>localStorage.getItem('state'));
        }

        if (!!state.user) {
            state.user = initialState.user;
        }

        actionInjection.commit(Mutations.stateInitialized, state);
        await actionInjection.dispatch(GetMe.name);
    }
}
