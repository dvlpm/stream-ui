import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import {inject, injectable} from "tsyringe";
import Authenticator from "@/Application/Authentication/Authenticator";
import router from "@/Infrastructure/Router/Router";
import GetMe from "@/Infrastructure/Store/Actions/User/GetMe";

@injectable()
export default class HandleAuthCallback implements ActionObject<State, State> {
    constructor(
        @inject(Authenticator) private authenticator: Authenticator
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        await this.authenticator.handleCallback().catch(() => {
            router.push({name: 'auth-page'});
        });
        await this.authenticator.getAccessToken().catch(() => {
            router.push({name: 'auth-page'});
        }).then((token: string | void) => {
            actionInjection.commit(Mutations.accessTokenUpdated, token);
            actionInjection.dispatch(GetMe.name).then(() => {
                router.push({name: 'profile-page'})
            })
        });
    }
}
