import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import PlanningPokerService from "@/Application/Service/PlanningPokerService";
import PlanningPoker from "@/Domain/Content/Model/PlanningPoker";
import {PlanningPokerStoryStatus} from "@/Domain/Content/Model/PlanningPokerStoryStatus";
import LoadInitialPlanningPokerStoriesPayload
    from "@/Infrastructure/Store/Actions/Room/LoadInitialPlanningPokerStoriesPayload";

@injectable()
export default class LoadInitialPlanningPokerStories implements ActionObject<State, State> {

    constructor(
        @inject(PlanningPokerService) private planningPokerService: PlanningPokerService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, payload: LoadInitialPlanningPokerStoriesPayload) => {
        await Promise.all([
            this.loadStoriesForPlanningPoker(actionInjection, payload.poker, PlanningPokerStoryStatus.ACTIVE, 1),
            this.loadStoriesForPlanningPoker(actionInjection, payload.poker, PlanningPokerStoryStatus.BACKLOG, 5),
            this.loadStoriesForPlanningPoker(actionInjection, payload.poker, PlanningPokerStoryStatus.COMPLETED, 2),
        ]);

        await actionInjection.commit(Mutations.roomUpdated, payload.room);
    }

    private async loadStoriesForPlanningPoker(
        actionInjection: ActionInjection,
        planningPoker: PlanningPoker,
        status: PlanningPokerStoryStatus,
        limit: number = 5,
        offset: number = 0
    ) {
        const stories = await this.planningPokerService.listStories(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            planningPoker.id,
            limit,
            offset,
            status
        );

        planningPoker.addStories(...stories);
    }
}
