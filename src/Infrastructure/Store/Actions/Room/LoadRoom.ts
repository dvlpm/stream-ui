import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import RoomService from "@/Application/Service/RoomService";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import LoadInitialPlanningPokerStories from "@/Infrastructure/Store/Actions/Room/LoadInitialPlanningPokerStories";
import LoadInitialPlanningPokerStoriesPayload
    from "@/Infrastructure/Store/Actions/Room/LoadInitialPlanningPokerStoriesPayload";

@injectable()
export default class LoadRoom implements ActionObject<State, State> {

    constructor(
        @inject(RoomService) private roomService: RoomService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, roomId: string) => {
        const room = await this.roomService.show(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            roomId
        );

        await actionInjection.commit(Mutations.roomUpdated, room);

        const poker = room.findPlanningPoker();
        if (poker !== null) {
            await actionInjection.dispatch(
                LoadInitialPlanningPokerStories.name,
                new LoadInitialPlanningPokerStoriesPayload(room, poker)
            );
        }
    }
}
