import Room from "@/Domain/Space/Model/Room";
import PlanningPoker from "@/Domain/Content/Model/PlanningPoker";

export default class LoadInitialPlanningPokerStoriesPayload {
    constructor(
        public room: Room,
        public poker: PlanningPoker,
    ) {
    }
}
