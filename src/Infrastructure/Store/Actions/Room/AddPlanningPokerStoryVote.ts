import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import PlanningPokerStoryService from "@/Application/Service/PlanningPokerStoryService";
import AddPlanningPokerStoryVotePayload from "@/Infrastructure/Store/Actions/Room/AddPlanningPokerStoryVotePayload";

@injectable()
export default class AddPlanningPokerStoryVote implements ActionObject<State, State> {

    constructor(
        @inject(PlanningPokerStoryService) private planningPokerStoryService: PlanningPokerStoryService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, payload: AddPlanningPokerStoryVotePayload) => {
        const story = await this.planningPokerStoryService.addVote(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            payload.story.id,
            payload.card
        );

        payload.poker.addStories(story);

        await actionInjection.commit(Mutations.roomUpdated, payload.room);
    }
}
