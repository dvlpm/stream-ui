import Room from "@/Domain/Space/Model/Room";
import PlanningPokerStory from "@/Domain/Content/Model/PlanningPokerStory";
import {PlanningPokerCard} from "@/Domain/Content/Model/PlanningPokerCard";
import PlanningPoker from "@/Domain/Content/Model/PlanningPoker";

export default class AddPlanningPokerStoryVotePayload {
    constructor(
        public room: Room,
        public poker: PlanningPoker,
        public story: PlanningPokerStory,
        public card: PlanningPokerCard
    ) {
    }
}