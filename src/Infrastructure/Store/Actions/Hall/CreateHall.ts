import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import HallService from "@/Application/Service/HallService";
import {Mutations} from "@/Infrastructure/Store/Mutations";
import CreateHallPayload from "@/Domain/Space/Payload/CreateHallPayload";

@injectable()
export default class CreateHall implements ActionObject<State, State> {

    constructor(
        @inject(HallService) private hallService: HallService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, payload: CreateHallPayload) => {
        const hall = await this.hallService.create(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            payload
        );

        await actionInjection.commit(Mutations.hallUpdated, hall);
    }
}
