import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import {inject, injectable} from "tsyringe";
import {ActionInjection} from "@/Infrastructure/Store/Actions";
import HallService from "@/Application/Service/HallService";
import {Mutations} from "@/Infrastructure/Store/Mutations";

@injectable()
export default class LoadHall implements ActionObject<State, State> {

    constructor(
        @inject(HallService) private hallService: HallService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, hallIdentifier: string) => {
        const hall = await this.hallService.show(
            actionInjection.getters.userId,
            actionInjection.getters.accessToken,
            hallIdentifier
        );

        await actionInjection.commit(Mutations.hallUpdated, hall);
    }
}
