import {MutationTree} from "vuex";
import {State} from "@/Infrastructure/Store/State";
import User from "@/Domain/User/Model/User";
import Hall from "@/Domain/Space/Model/Hall";
import Room from "@/Domain/Space/Model/Room";

export enum Mutations {
    stateInitialized = 'stateInitialized',
    accessTokenUpdated = 'accessTokenUpdated',
    userUpdated = 'userUpdated',
    guestIdUpdated = 'guestIdUpdated',
    hallUpdated = 'hallUpdated',
    managedHallsUpdated = 'managedHallsUpdated',
    roomUpdated = 'roomUpdated',
}

export default (): MutationTree<State> => {
    return {
        [Mutations.stateInitialized](state: State, sourceState: State): void {
            this.replaceState(Object.assign(state, sourceState));
            state.isInitialized = true
        },
        [Mutations.accessTokenUpdated](state: State, accessToken: string): void {
            state.accessToken = accessToken;
        },
        [Mutations.userUpdated](state: State, user: User): void {
            state.user = user;
        },
        [Mutations.guestIdUpdated](state: State, guestId: string): void {
            state.guestId = guestId;
        },
        [Mutations.hallUpdated](state: State, hall: Hall | null): void {
            state.hall = hall;
        },
        [Mutations.managedHallsUpdated](state: State, managedHalls: Hall[]): void {
            state.managedHalls = managedHalls;
        },
        [Mutations.roomUpdated](state: State, room: Room | null): void {
            state.room = room;
        },
    }
}
