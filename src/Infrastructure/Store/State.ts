import User from "@/Domain/User/Model/User";
import Hall from "@/Domain/Space/Model/Hall";
import Room from "@/Domain/Space/Model/Room";

export interface State {
    version: string
    isInitialized: boolean
    accessToken: string
    user: User
    guestId: string
    hall: Hall | null
    managedHalls: Hall[]
    room: Room | null
}

export const emptyState = (): State => {
    return {
        version: '',
        isInitialized: false,
        accessToken: '',
        user: User.new(),
        guestId: '',
        hall: null,
        managedHalls: [],
        room: null
    }
};
