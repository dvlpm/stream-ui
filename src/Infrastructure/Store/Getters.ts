import {State} from "@/Infrastructure/Store/State";
import {GetterTree} from "vuex";
import User from "@/Domain/User/Model/User";
import Hall from "@/Domain/Space/Model/Hall";
import Room from "@/Domain/Space/Model/Room";

export interface GettersInterface extends GetterTree<State, State> {
    userId(...params: any): string;
    guestId(...params: any): string;
    accessToken(...params: any): string;
    user(...params: any): User;
    hall(...params: any): Hall | null;
    room(...params: any): Room | null;
    isAuthenticated(...params: any): boolean;
    isInitialized(...params: any): boolean;
}

export type Getters = {
    readonly [P in keyof GettersInterface]: ReturnType<GettersInterface[P]>;
};

export default (): GettersInterface => {
    return {
        userId(state: State): string {
            return state.user.id;
        },
        guestId(state: State): string {
            return state.guestId;
        },
        accessToken(state: State): string {
            return state.accessToken;
        },
        user(state: State): User {
            return state.user;
        },
        hall(state: State): Hall | null {
            return state.hall;
        },
        managedHalls(state: State): Hall[] {
            return state.managedHalls;
        },
        room(state: State): Room | null {
            return state.room;
        },
        isAuthenticated(state: State): boolean {
            return !!state.accessToken && !!state.user.id;
        },
        isInitialized(state: State): boolean {
            return state.isInitialized;
        },
    }
}
