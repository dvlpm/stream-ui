import {ActionTree, Commit, Dispatch} from "vuex";
import {Getters} from "@/Infrastructure/Store/Getters";
import {State} from "@/Infrastructure/Store/State";
import {container} from "tsyringe";
import InitializeState from "@/Infrastructure/Store/Actions/InitializeState";
import HandleAuthCallback from "@/Infrastructure/Store/Actions/HandleAuthCallback";
import LoginWithRedirect from "@/Infrastructure/Store/Actions/LoginWithRedirect";
import GetMe from "@/Infrastructure/Store/Actions/User/GetMe";
import ForgetMe from "@/Infrastructure/Store/Actions/User/ForgetMe";
import LoadHall from "@/Infrastructure/Store/Actions/Hall/LoadHall";
import LoadRoom from "@/Infrastructure/Store/Actions/Room/LoadRoom";
import LoadInitialPlanningPokerStories from "@/Infrastructure/Store/Actions/Room/LoadInitialPlanningPokerStories";
import AddPlanningPokerStoryVote from "@/Infrastructure/Store/Actions/Room/AddPlanningPokerStoryVote";
import CreateHall from "@/Infrastructure/Store/Actions/Hall/CreateHall";
import LoadManagedHalls from "@/Infrastructure/Store/Actions/ManagedHalls/LoadManagedHalls";

export interface ActionInjection {
    readonly dispatch: Dispatch
    readonly getters: Getters
    readonly commit: Commit
    readonly state: State
}

export default (): ActionTree<State, State> => {
    return {
        [InitializeState.name]: container.resolve(InitializeState),
        [HandleAuthCallback.name]: container.resolve(HandleAuthCallback),
        [LoginWithRedirect.name]: container.resolve(LoginWithRedirect),
        [GetMe.name]: container.resolve(GetMe),
        [ForgetMe.name]: container.resolve(ForgetMe),
        [LoadHall.name]: container.resolve(LoadHall),
        [LoadRoom.name]: container.resolve(LoadRoom),
        [LoadInitialPlanningPokerStories.name]: container.resolve(LoadInitialPlanningPokerStories),
        [AddPlanningPokerStoryVote.name]: container.resolve(AddPlanningPokerStoryVote),
        [CreateHall.name]: container.resolve(CreateHall),
        [LoadManagedHalls.name]: container.resolve(LoadManagedHalls),
    };
}
