import Vue from 'vue';
import Vuex, {MutationPayload} from 'vuex';
import {emptyState, State} from "@/Infrastructure/Store/State";
import mutations from "@/Infrastructure/Store/Mutations";
import actions from "@/Infrastructure/Store/Actions";
import getters from "@/Infrastructure/Store/Getters";

Vue.use(Vuex);

export const store = new Vuex.Store<State>({
    state: emptyState(),
    mutations: mutations(),
    actions: actions(),
    getters: getters()
});

store.subscribe((mutation: MutationPayload, state: State): void => {
    let storableState = {
        version: state.version,
        accessToken: state.accessToken,
        user: {
            id: state.user.id,
        },
        guestId: state.guestId
    };

    localStorage.setItem('state', JSON.stringify(storableState));
});
