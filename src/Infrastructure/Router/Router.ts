import Vue from 'vue';
import VueRouter, {Route} from 'vue-router';
import MainPage from "@/Infrastructure/Pages/MainPage.vue";
import "reflect-metadata";
import EnterHallPage from "@/Infrastructure/Pages/EnterHallPage.vue";
import AuthPage from "@/Infrastructure/Pages/AuthPage.vue";
import AuthCallbackPage from "@/Infrastructure/Pages/AuthCallbackPage.vue";
import ProfilePage from "@/Infrastructure/Pages/ProfilePage.vue";
import {store} from '@/Infrastructure/Store/Store';
import HallPage from "@/Infrastructure/Pages/HallPage.vue";
import InitializeState from "@/Infrastructure/Store/Actions/InitializeState";
import RoomPage from "@/Infrastructure/Pages/RoomPage.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'main-page',
        component: MainPage,
        meta: {
            title: 'Streaming home page'
        },
        beforeEnter: (to: Route, from: Route, next: any) => {
            if (store.getters.isAuthenticated) {
                next({name: 'profile-page'})

                return;
            }
            next({name: 'auth-page'})
        },
    },
    {
        path: '/auth',
        name: 'auth-page',
        component: AuthPage,
        meta: {
            title: 'Auth page'
        },
    },
    {
        path: '/auth-callback',
        name: 'auth-callback-page',
        component: AuthCallbackPage,
        meta: {
            title: 'Auth page'
        },
    },
    {
        path: '/profile',
        name: 'profile-page',
        component: ProfilePage,
        meta: {
            title: 'Profile page',
            authRequired: true,
        },
    },
    {
        path: '/hall',
        name: 'enter-hall-page',
        component: EnterHallPage,
        meta: {
            title: 'Enter hall'
        },
    },
    {
        path: '/hall/:hallIdentifier',
        name: 'hall-page',
        component: HallPage,
        meta: {
            title: 'Hall page',
        },
    },
    {
        path: '/room/:roomId',
        name: 'room-page',
        component: RoomPage,
        meta: {
            title: 'Room page',
        },
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title

    if (store.getters.isInitialized) {
        if (to.meta.authRequired && !store.getters.isAuthenticated) {
            next({name: 'auth-page'})
        }

        next()
    }

    store.dispatch(InitializeState.name).then(() => {
        if (to.meta.authRequired && !store.getters.isAuthenticated) {
            next({name: 'auth-page'})
        }

        next()
    })
})

export default router;
