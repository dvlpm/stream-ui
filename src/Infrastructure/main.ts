import "reflect-metadata";
import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from '@/Infrastructure/Router/Router';
import {store} from '@/Infrastructure/Store/Store';
import vuetify from '@/Infrastructure/Plugins/Vuetify';
import "emoji-mart-vue-fast/css/emoji-mart.css";
import 'vue2-datepicker/index.css';

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App),
}).$mount('#app');
