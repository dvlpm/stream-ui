# UI

## Development

### Certs MacOs

If you want to use valid certificates on your mac use:

```
source .env
brew install mkcert
mkcert -install
mkcert $SERVER_NAME
mv $SERVER_NAME.pem ./volumes/nginx/etc/nginx/certs/cert.pem
mv $SERVER_NAME-key.pem ./volumes/nginx/etc/nginx/certs/cert.pem
```
